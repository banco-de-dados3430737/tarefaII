package exemploCRUD;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private ExemploCRUD operacoesCRUD;
    static Scanner scanner = new Scanner(System.in);

    public Menu() {
        operacoesCRUD = new ExemploCRUD();
    }

    public void executar() {
        int escolha;
        do {
            exibirMenu();
            escolha = scanner.nextInt();
            scanner.nextLine();

            switch (escolha) {
                case 1:
                    inserirPessoa();
                    break;
                case 2:
                    atualizarPessoa();
                    break;
                case 3:
                    listarPessoas();
                    break;
                case 4:
                    excluirPessoa();
                    break;
                case 5:
                    System.out.println("Saindo do programa.");
                    break;
                default:
                    System.out.println("Opção inválida. Tente novamente.");
                    break;
            }

        } while (escolha != 7);
        scanner.close();
        operacoesCRUD.fecharConexao();
    }

    public void exibirMenu() {
        System.out.println("Menu:");
        System.out.println("1 - Inserir Pessoa");
        System.out.println("2 - Atualizar Pessoa");
        System.out.println("3 - Listar Pessoas");
        System.out.println("4 - Excluir Pessoa");
        System.out.println("5 - Sair");
        System.out.print("Escolha uma opção: ");
    }

    public void inserirPessoa() {
        System.out.print("Nome: ");
        String nome = scanner.nextLine();
        System.out.print("Sobrenome: ");
        String sobrenome = scanner.nextLine();
        System.out.print("Idade: ");
        int idade = scanner.nextInt();
        operacoesCRUD.inserirPessoa(nome, sobrenome, idade);
        System.out.println("Pessoa inserida com sucesso.");
    }

    public void atualizarPessoa() {
        System.out.print("ID da Pessoa a ser atualizada: ");
        int idPessoa = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Novo Nome: ");
        String nome = scanner.nextLine();
        System.out.print("Novo Sobrenome: ");
        String sobrenome = scanner.nextLine();
        System.out.print("Nova Idade: ");
        int idade = scanner.nextInt();
        operacoesCRUD.atualizarPessoa(idPessoa, nome, sobrenome, idade);
        System.out.println("Pessoa atualizada com sucesso.");
    }

    public void listarPessoas() {
        List<Pessoa> pessoas = operacoesCRUD.listarPessoas();
        if (pessoas.isEmpty()) {
            System.out.println("Nenhuma pessoa encontrada.");
        } else {
            System.out.println("Lista de Pessoas:");
            for (Pessoa pessoa : pessoas) {
                System.out.println("ID: " + pessoa.getIdPessoa() + ", Nome: " + pessoa.getNome() + ", Sobrenome: "
                        + pessoa.getSobrenome() + ", Idade: " + pessoa.getIdade());
            }
        }
    }

    public void excluirPessoa() {
        System.out.print("ID da Pessoa a ser excluída: ");
        int idPessoa = scanner.nextInt();
        operacoesCRUD.excluirPessoa(idPessoa);
        System.out.println("Pessoa excluída com sucesso.");
    }

    

}
