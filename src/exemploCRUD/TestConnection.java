
package exemploCRUD;

import javax.swing.JOptionPane;

public class TestConnection {
    
    public static void main(String[] args) {
        try {
            new ExemploCRUD();
            JOptionPane.showMessageDialog(null, "Conectado com sucesso!");
        } catch (Exception error) {
            JOptionPane.showMessageDialog(null, "Erro ao conectar: " + error);
        }
    }
}
