package exemploCRUD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class ExemploCRUD {

	private Connection conn;

	public ExemploCRUD() {
		String url = "jdbc:mysql://localhost:SUA_PORTA_DO_BANCO_DE_DADOS/cadastroPessoa";
		String usuario = "SEU_USUARIO";
		String senha = "SUA_SENHA";

		try {
			conn = DriverManager.getConnection(url, usuario, senha);
		} catch (SQLException e) {
			throw new RuntimeException("Não foi possível conectar ao banco de dados.");
		}
	}

	public void inserirPessoa(String nome, String sobrenome, int idade) {
		String sql = "INSERT INTO Pessoa (nome, sobrenome, idade) VALUES (?,?,?)";

		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, nome);
			stmt.setString(2, sobrenome);
			stmt.setInt(3, idade);
			stmt.executeUpdate();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir pessoa no banco de dados: " + e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void atualizarPessoa(int idPessoa, String nome, String sobrenome, int idade) {
		String sql = "UPDATE Pessoa SET nome = ?, sobrenome = ?, idade = ? WHERE idPessoa = ?";

		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, nome);
			stmt.setString(2, sobrenome);
			stmt.setInt(3, idade);
			stmt.setInt(4, idPessoa);
			int atualizado = stmt.executeUpdate();

			if (atualizado > 0) {
				JOptionPane.showMessageDialog(null, "Pessoa atualizada com sucesso.");
			} else {
				JOptionPane.showMessageDialog(null, "Nenhuma pessoa encontrada com o ID especificado.", "Erro",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao atualizar pessoa no banco de dados: " + e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		}
	}

	public List<Pessoa> listarPessoas() {
		String sql = "SELECT * FROM Pessoa";
		List<Pessoa> pessoas = new ArrayList<>();

		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int idPessoa = rs.getInt("idPessoa");
				String nome = rs.getString("nome");
				String sobrenome = rs.getString("sobrenome");
				int idade = rs.getInt("idade");

				pessoas.add(new Pessoa(idPessoa, nome, sobrenome, idade));
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao listar pessoas do banco de dados: " + e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		}

		return pessoas;
	}

	public void excluirPessoa(int idPessoa) {
		String sql = "DELETE FROM Pessoa WHERE idPessoa = ?";

		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setInt(1, idPessoa);
			int excluido = stmt.executeUpdate();

			if (excluido > 0) {
				JOptionPane.showMessageDialog(null, "Pessoa excluída com sucesso.");
			} else {
				JOptionPane.showMessageDialog(null, "Nenhuma pessoa encontrada com o ID especificado.", "Erro",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir pessoa do banco de dados: " + e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}


	public void fecharConexao() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao fechar a conexão com o banco de dados: " + e.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
		}
	}

}